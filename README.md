**Introduction**

[darkhttpd](https://unix4lyfe.org/darkhttpd/) is simple web server serving static files only. Except from manual:

```
usage:	darkhttpd /path/to/wwwroot [flags]
flags:	--port number (default: 8080, or 80 if running as root)
		Specifies which port to listen on for connections.
		Pass 0 to let the system choose any free port for you.
....
```

**Task**

1. Your task is to run website on [localhost on port 8080](http://localhost:8080).
2. Site should be served by darkhttpd run from Docker container.
3. Content of the website is compressed in html5up-spectral.zip file.
4. Docker image should be build from provided Dockerfile.

__But be careful: there is error in Dockerfile, so you should fix it first__