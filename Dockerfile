FROM centos:latest

LABEL maintainer="Maciej Ostaszewski <maciej.ostaszewski@hrs.com>"

RUN yum install -y epel-release \
    &&  yum install -y darkhttpd \
    && mkdir /www \
    && echo "Darkhttpd is working" > /www/index.html

WORKDIR /www

CMD=["darkhttpd","."]

